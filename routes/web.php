<?php

//Navigate through pages...

Route::get('/', function () {
    return view('userend.index');
});
Route::get('/products',function(){
    return view('userend.products');
});
Route::get('/about-us',function(){
    return view('userend.about-us');
});
Route::get('/contact',function(){
    return view('userend.contact');
});

Route::get('nl',function(){
    return view('userend.layouts.new-layout');
});