@extends('userend.layouts.main-layout')

@section('content')
    <h1 class="products-heading font-heading">Products</h1>
    <hr>

    <div class="products">
        <div class="products-description col-md-12 col-sm-12">
            <p class="font-common">In publishing and graphic design, lorem ipsum is a placeholder text commonly used to demonstrate the
                visual form of a document without relying on meaningful content. Replacing the actual content with
                placeholder text allows designers to design the form of the content before the content itself has been
                produced.In publishing and graphic design, lorem ipsum is a placeholder text commonly used to
                demonstrate the visual form of a document without relying on meaningful content. Replacing the actual
                content with placeholder text allows designers to design the form of the content before the content
                itself has been produced.</p>
        </div>

        <div class="home-products col-md-12 col-sm-12">
            <div class="row">
                @for($i=1;$i<10;$i++)
                    <div class="col-md-4 col-sm-12">
                        @include('userend.includes.product-single-block')
                    </div>
                @endfor
            </div>
        </div>
        <div class="products-view-more font-common col-md-12 col-sm-12">
            <a href="{{url('/products')}}" class="btn btn-info btn-lg">View More</a>
        </div>
    </div>
@endsection