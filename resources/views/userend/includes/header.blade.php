<div class="header col-md-12 col-sm-12">
    <div class="row header-inner">
        <div class="header-left col-md-4 col-sm-12">
            <img src="http://pluspng.com/img-png/png-logo-design-do-you-know-the-importance-of-an-easy-to-recognize-logo-design-let-centralync-help-you-design-a-logo-that-allows-potential-clients-to-really-notice-your-676.png"
                 width="300" height="100" alt="LOGO"/>
        </div>
        <div class="header-right col-md-8 col-sm-12">
            <h1 class="header-heading">Spicy Land</h1>
            <p class="header-heading header-heading-2">Sri Lankan black pepper and spices farmers and exporter</p>
        </div>
    </div>
    <div class="row header-nav">
        <hr>
        <div class="nav-bar">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex flex-wrap justify-content-between align-items-center">
                    <nav class="site-navigation d-flex justify-content-end align-items-center">
                            <ul class="d-flex flex-column flex-lg-row justify-content-lg-end align-content-center">
                                <li class="current-menu-item"><a href="index.html">HOME</a></li>
                                <li><a href="">PRODUCTS</a></li>
                                <li><a href="">ABOUT US</a></li>
                                <li><a href="">CONTACT</a></li>
                            </ul>
                        </nav><!-- .site-navigation -->

                        <div class="hamburger-menu d-lg-none">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div><!-- .hamburger-menu -->
                    </div><!-- .col -->
                </div><!-- .row -->
            </div><!-- .container -->
        </div><!-- .nav-bar -->

        <hr>
    </div>
</div>
<div class="col-md-12 col-sm-12">
    <div class="row">
        <div class="col-md-3">&nbsp;</div>
        <div class="col-md-6 col-sm-12">
            {{--Searchbar--}}
            <div class="header-search col-md-12 col-sm-12">
                <input type="text" class="header-search-text font-common form-control" placeholder="Search..."/>
            </div>
        </div>
    </div>
</div>