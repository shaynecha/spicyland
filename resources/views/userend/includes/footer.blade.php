<div class="footer col-md-12 col-sm-12">
    <div class="row">
        <div class="footer-logo col-md-4 col-sm-12">
            <img src="http://pluspng.com/img-png/png-logo-design-do-you-know-the-importance-of-an-easy-to-recognize-logo-design-let-centralync-help-you-design-a-logo-that-allows-potential-clients-to-really-notice-your-676.png" width="300" height="100" alt="LOGO"/>
        </div>
        <div class="col-md-4 col-sm-12">
            © 2019 Spicy Land. All rights reserved.<br>
            <a href="#">Privacy Policy</a> |
            <a href="#">Terms & Conditions</a>
        </div>
        <div class="col-md-4 col-sm-12">
            <h4>Connect with us:</h4>
            <img src="https://cdn1.iconfinder.com/data/icons/iconza-circle-social/64/697057-facebook-512.png" width="48" height="48" alt="fb"/>
            <img src="https://cdn2.iconfinder.com/data/icons/social-media-2102/100/social_media_network-16-512.png" width="48" height="48" alt="fb"/>
            <img src="https://www.shareicon.net/data/512x512/2015/09/04/95565_yt_512x512.png" width="48" height="48" alt="fb"/>
            {{--<img src="https://cdn0.iconfinder.com/data/icons/Pinterest/512/Pinterest_Favicon.png" width="32" height="32" alt="fb"/>--}}
            {{--<img src="https://cdn0.iconfinder.com/data/icons/social-circle/512/587255-tumblr-512.png" width="32" height="32" alt="fb"/>--}}
        </div>
    </div>
</div>