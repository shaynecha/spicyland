<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{asset('favicon.ico')}}" rel="icon" type="image/ico">
    <link rel="stylesheet" href="{{asset('css/my-styles.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" type="text/css">

    <link href="https://fonts.googleapis.com/css?family=Varela+Round&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Work+Sans&display=swap" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/elegant-fonts.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/themify-icons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/swiper.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">

    @yield('styles')
</head>
<body class="main-body">
@include('userend.includes.header')
<div>
    @yield('content')
</div>
@include('userend.includes.footer')
</body>
</html>

<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/my-javascripts.js')}}"></script>
<script>$('.carousel').carousel()</script>

<script type='text/javascript' src='{{asset('js/jquery.js')}}'></script>
<script type='text/javascript' src='{{asset('js/jquery.collapsible.min.js')}}'></script>
<script type='text/javascript' src='{{asset('js/swiper.min.js')}}'></script>
<script type='text/javascript' src='{{asset('js/jquery.countdown.min.js')}}'></script>
<script type='text/javascript' src='{{asset('js/circle-progress.min.js')}}'></script>
<script type='text/javascript' src='{{asset('js/jquery.countTo.min.js')}}'></script>
<script type='text/javascript' src='{{asset('js/jquery.barfiller.js')}}'></script>
<script type='text/javascript' src='{{asset('js/custom.js')}}'></script>